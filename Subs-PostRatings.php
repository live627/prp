<?php
// Version: 1.0: PostRatings.php

if (!defined('SMF'))
	die('Hacking attempt...');

function pr_admin_areas(&$admin_areas)
{
	global $txt;

	$admin_areas['layout']['areas']['postratings'] = array(
		'label' => $txt['post_ratings_settings'],
		'file' => 'ManagePostRatings.php',
		'function' => 'ManagePostRatings',
		'icon' => 'languages.gif',
		'subsections' => array(
			'general' => array($txt['post_ratings_settings_general']),
			'boards' => array($txt['post_ratings_settings_boards']),
			'i' => array('Import'),
		),
	);
}

function pr_pre_load()
{
	loadLanguage('PostRatings');

	add_integration_function('integrate_load_theme', 'pr_load_theme', false);
}

function pr_load_theme()
{
	global $board, $modSettings, $ratings_setting, $smcFunc;

	if (isset($board))
	{
		$request = $smcFunc['db_query']('', '
			SELECT variable, value
			FROM {db_prefix}ratings_settings
			WHERE id_board = {int:board}',
			array(
				'board' => $board,
			)
		);
		while ($row = $smcFunc['db_fetch_row']($request))
			$modSettings[$row[0]] = $row[1];
		$smcFunc['db_free_result']($request);

		if (isset($modSettings['post_ratings']))
			$ratings_setting = unserialize($modSettings['post_ratings']);

		// Compatibility...
		$modSettings['post_ratings_level'] = 1;
	}
}

function pr_load_permissions(&$permissionGroups, &$permissionList, &$leftPermissionGroups, &$hiddenPermissions, &$relabelPermissions)
{
	global $context;

	$permissionList['board'] += array(
		'postratings_view' => array(false, 'postratings', 'postratings'),
		'postratings_rate' => array(false, 'postratings', 'postratings'),
		'postratings_edit_own_rating' => array(false, 'postratings', 'postratings'),
		'postratings_delete_own_rating' => array(false, 'postratings', 'postratings'),
		'postratings_administrate' => array(false, 'postratings', 'postratings'),
		'ratings_enable_topic' => array(true, 'topic', 'moderate', 'moderate'),
		'ratings_enable_post' => array(true, 'post', 'modify', 'moderate'),
	);

	$context['non_guest_permissions'] = array_unshift($context['non_guest_permissions'],
		'postratings_rate',
		'postratings_edit_own_rating',
		'postratings_delete_own_rating',
		'postratings_administrate'
	);
}

function pr_set_permission_level(&$groupLevels, &$boardLevels)
{
	$groupLevels['board']['standard'] += array(
		'postratings_view',
		'postratings_rate',
		'postratings_edit_own_rating',
		'postratings_delete_own_rating',
		'postratings_administrate',
		'ratings_enable_topic_own',
		'ratings_enable_post_own'
	);

	$groupLevels['board']['moderator'] += array(
		'ratings_enable_topic_any',
		'ratings_enable_post_any'
	);

	$boardLevels['publish'] += array(
		'ratings_enable_topic_own',
		'ratings_enable_post_own'
	);

	$boardLevels['free'] += array(
		'ratings_enable_topic_any',
		'ratings_enable_post_any'
	);
}

function pr_actions(&$actions)
{
	$actions['postratings'] = array('PostRatings.php', 'PostRatings');
}

function load_ratings_info($id_msg, $enabled, $last_rating, $posterId, $is_ratings)
{
	global $context, $ratings, $user_info, $modSettings, $txt, $scripturl, $topic;

	$ratingsData = array();

	$already_rated = false;
	$has_last_rating = false;

	// only do this if enabled in Admin Settings and isset and enabled in topic settings.
	if (isset($ratings[$id_msg]) && !empty($modSettings['post_ratings_enable']) && !empty($is_ratings))
	{
		$gatherInfo = array();

		foreach ($ratings[$id_msg] as $i => $rating)
		{
			if (!empty($modSettings['post_ratings_show_lastratedtime']) && allowedTo('postratings_view'))
				if ($last_rating == $rating['id_rating'])
				{
					$has_last_rating = true;
					$gatherInfo['last_rated_time'] = $rating['date'];
				}
				else
				{
					if (!isset($gatherInfo['last_rated_time']) || empty($gatherInfo['last_rated_time']))
					{
						$has_last_rating = false;
						$gatherInfo['last_rated_time'] = 0;
					}
				}


			if (!empty($is_ratings) && (allowedTo('postratings_rate') || allowedTo('postratings_edit_own_rating') || allowedTo('postratings_delete_own_rating')))
			// already rated?
			if ($user_info['id'] == $rating['id_member'])
			{
				$already_rated = true;
				$gatherInfo['value'] = $rating['value'];
			}
			else
			{
				if (!isset($gatherInfo['value']) || empty($gatherInfo['value']))
				{
					$already_rated = false;
					$gatherInfo['value'] = -1;
				}
			}
		}

		$ratingsData = array(
			// returns true if you are allowed to edit a rating, if the post is enabled, you aren't the poster, and you already rated this post!
			'can_postratings_edit' => allowedTo('postratings_edit_own_rating') && !empty($enabled) && $user_info['id'] != $posterId && $already_rated,
			// returns true if you are allowed to delete own rating, if the post is enabled, you aren't the poster, and you already rated this post!
			'can_delete_own_rating' => allowedTo('postratings_delete_own_rating') && !empty($enabled) && $already_rated && $user_info['id'] != $posterId,
			'already_rated' => $already_rated ? 1 : 0,
			'last_rated_time' => $has_last_rating ? timeformat($gatherInfo['last_rated_time']) : 0,
			'your_rating' => $already_rated ? $gatherInfo['value'] : -1,
		);
	}

	if (!empty($ratingsData))
		return $ratingsData;
}

function get_post_rating($type = 'text', $msgId = 0, $totalratings, $rating)
{
	global $boardurl, $modSettings, $options, $smcFunc;

	if (empty($msgId) || !empty($options['ratedisable']))
		return 0;

	$rating_str = '';
	$nonratedImg = $boardurl . '/' . $modSettings['post_ratings_image_nonrated'];
	$ratedImg = $boardurl . '/' . $modSettings['post_ratings_image_rated'];

	$ratings_setting = unserialize($modSettings['post_ratings']);
	$alt = ' title="Rating: ' . round($rating, 2) . '" style="vertical-align: middle;"';

	if (empty($totalratings) && $rating == -1)
		return parse_bbc($modSettings['post_ratings_not_rated']);

	if ($type == 'stars')
	{
		$rating_str = str_repeat('
										<img src="' . $ratedImg . '" border="0"' . $alt . ' />', round($rating) + 1);
		$rating_str .= str_repeat('
										<img src="' . $nonratedImg . '" border="0"' . $alt . ' />', count($ratings_setting) - (round($rating) + 1));
		return $rating_str;
	}
	else
		return round($rating);
}

function pr_stars_input($message, $rating, $can_interact)
{
	global $boardurl, $modSettings, $options;

	if (empty($message) || !empty($options['ratedisable']))
		return '';

	$nonratedImg = $boardurl . '/' . $modSettings['post_ratings_image_nonrated'];
	$ratedImg = $boardurl . '/' . $modSettings['post_ratings_image_rated'];
	$ret = '';

	$ratings_setting = unserialize($modSettings['post_ratings']);
	foreach ($ratings_setting as $k => $v)
	{
		if (true)
		{
			if ($k <= $rating)
				$src = $ratedImg;
			else
				$src = $nonratedImg;
		}
		if ($can_interact)
			$ret .= '
										<img src="' . $src . '" alt="' . $v['text'] . '" title="' . $v['text'] . '" class="pr" id="rating_' . $k . '_' . $message . '" style="vertical-align: middle;" />';
		else
			$ret .= '
										<img src="' . $src . '" alt="' . $v['text'] . '" title="' . $v['text'] . '" style="vertical-align: middle;" />';
	}
	return $ret;
}

function pr_remove_topics($topics)
{
	global $smcFunc;

	// Get all messages to delete ratings from according to the topic id
	$request = $smcFunc['db_query']('', '
		SELECT id_msg
		FROM {db_prefix}messages
		WHERE id_topic IN ({array_int:topics})',
		array(
			'topics' => $topics,
		)
	);
	$messages = array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$messages[] = $row['id_msg'];

	$smcFunc['db_free_result']($request);

	// Delete all ratings within all messages for this topic.
	$smcFunc['db_query']('', '
		DELETE FROM {db_prefix}log_message_ratings
		WHERE id_msg IN ({array_int:messages})',
		array(
			'messages' => $messages,
		)
	);
}

function pr_remove_messages($message)
{
	global $smcFunc;

	// Remove any ratings in the message!
	$smcFunc['db_query']('', '
		DELETE FROM {db_prefix}log_message_ratings
		WHERE id_msg = {int:message}',
		array(
			'message' => $message,
		)
	);
}

function integrate_post_ratings_display($messages)
{
	global $boardurl, $context, $modSettings, $ratings, $settings, $user_info;
	add_integration_function('integrate_mod_buttons', 'pr_mod_buttons', false);
	add_integration_function('integrate_buffer', 'pr_buffer_display', false);
	$rate_text = $rate_stars = array();

	// ok, set function for output of mouseover events when rating a post...
	$ratings_setting = unserialize($modSettings['post_ratings']);
	foreach ($ratings_setting as $k => $v)
		$rate_text[$k] = parse_bbc($v['text'], true);
	foreach ($messages as $message)
	{
		if (!empty($ratings[$message]))
			foreach ($ratings[$message] as $k => $v)
				if ($user_info['id'] == $v['id_member'])
				{
					$v['your_rating'] = $v['value'];
					$rate_stars[(int) $message] = $v['your_rating'];
				}
				else
					$rate_stars[$message] = -1;
		else
			$rate_stars[$message] = -1;
	}

	$context['html_headers'] .= '
		<script language="JavaScript" type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/postratings.js"></script>
		<link rel="stylesheet" type="text/css" href="' . $settings['default_theme_url'] . '/css/postratings.css?rc3" />';
	$context['insert_after_template'] = '
				<script type="text/javascript"><!-- // --><![CDATA[
					var oPostRatings = new postRatings({
						sSelf: \'oPostRatings\',
						iTopic: \'' . $context['current_topic'] . '\',
						sSessionId: \'' . $context['session_id'] . '\',
						sSessionVar: \'' . $context['session_var'] . '\',
						rateText: ' . json_encode($rate_text) . ',
						oStars: ' . json_encode($rate_stars) . ',
						imageOut: ' . JavaScriptEscape($boardurl . '/' . $modSettings['post_ratings_image_nonrated']) . ',
						imageOver: '  . JavaScriptEscape($boardurl . '/' . $modSettings['post_ratings_image_rated']) . '
					});
				// ]]></script>';
}

function pr_mod_buttons(&$mod_buttons)
{
	global $context, $scripturl;

	$mod_buttons += array(
		'ratings_enable_topic' => array('test' => 'can_ratings_enable_topic', 'text' => $context['pr_is_topic_enabled'] ? 'disable_topic_ratings' : 'enable_topic_ratings', 'lang' => true, 'url' => $scripturl . '?action=postratings;sa=' . ($context['pr_is_topic_enabled'] ? 'disabletopic' : 'enabletopic') . ';topic=' . $context['current_topic'] . ';' . $context['session_var'] . '=' . $context['session_id']),
		'postratings_delete' => array('test' => 'can_postratings_delete', 'text' => 'delete_topic_ratings', 'lang' => true, 'url' => $scripturl . '?action=postratings;sa=deleteall;topic=' . $context['current_topic']),
	);
}

function integrate_post_ratings_display_context(&$message)
{
	global $boardurl, $context, $modSettings, $scripturl, $settings, $topicinfo, $txt, $user_info, $options;

	if (!empty($options['ratedisable']))
		return;

	$ratings_setting = unserialize($modSettings['post_ratings']);
	if (!empty($modSettings['post_ratings_show_lastratedtime']))
		if ($message['ratings_enabled'] && $message['first_enabled'] && $message['topic_enabled'] && allowedTo('postratings_view') && $message['has_ratings'] && !empty($message['rating']['last_rated_time']))
			$message['time'] .= ' (' . $txt['last_rated_time'] . ' ' . $message['rating']['last_rated_time'] . ')';

	if (!empty($modSettings['post_ratings_topic_only']) && !empty($modSettings['post_ratings_first_post']))
		$message['first_enabled'] = $message['id'] == $topicinfo['id_first_msg'];

		// Header Style?
		if (!empty($modSettings['post_ratings_layout_style']))
		{
			// Header - is it enabled or not??
			if ($message['ratings_enabled'] && $message['topic_enabled'] && $message['first_enabled'] && allowedTo('postratings_view'))
			{
				$pr_head = '
				<div class="title_bar">
					<h3 class="titlebg" style="font-weight: normal;">';

				if ($message['has_ratings'])
				{
					if (!empty($modSettings['post_ratings_show_quantity']))
						$pr_head .= '<div class="floatright smalltext">' . $txt['postratings_count1'] . ' <strong>' . $message['post_rating_totals'] . '</strong> ' . ($message['post_rating_totals'] < 2 ? $txt['postratings_count2'] : $txt['postratings_count3']) . '</div>';

					$pr_head .= $message['post_rating_stars'] . parse_bbc(stripslashes($ratings_setting[$message['post_rating_num']]['text']));

				}
				else
					$pr_head .= parse_bbc(stripslashes($modSettings['post_ratings_not_rated']));

				$context['pr_head'][$message['id']] = $pr_head . '
					</h3>
				</div>';
			}
		}

		// Inner Style?
		if(empty($modSettings['post_ratings_layout_style']))
		{
			// is it enabled or not??
			if ($message['ratings_enabled'] && $message['topic_enabled'] && $message['first_enabled'] && allowedTo('postratings_view'))
			{
				$pr_body = '
									<div>';

				// does the post have any ratings yet?
				if (!$message['has_ratings'])
					$pr_body .= parse_bbc($modSettings['post_ratings_not_rated']);
				else
				{
					$pr_body .= $message['post_rating_stars'] . '
										' . parse_bbc($ratings_setting[$message['post_rating_num']]['text'], true);

					if (!empty($modSettings['post_ratings_show_quantity']))
						$pr_body .= '
										<div class="smalltext floatright">' . $txt['postratings_count1'] . ' <strong>' . $message['post_rating_totals'] . '</strong> ' . ($message['post_rating_totals'] < 2 ? $txt['postratings_count2'] : $txt['postratings_count3']) . '</div>';
				}
				$message['body'] = $pr_body . '
									</div>
									' . $message['body'];
			}
		}

		if (!$context['is_ratings_locked'])
		{
			if ($message['can_postratings_rate'] || !empty($message['rating']) && !$user_info['is_guest'])
			{
				$is_enabled = $message['ratings_enabled'] && $message['topic_enabled'] && $message['first_enabled'] ? true : false;

				// Is it the POST CREATOR
				if ($is_enabled && $message['not_your_post'])
				{
					// only show it if we need it for anything...
					if (($message['can_postratings_rate'] && !$message['reached_ratings_limit']) || $message['rating']['can_postratings_edit'] || $message['rating']['can_delete_own_rating'])
					{
						$can_delete_only = false;

						$message['member']['custom_fields']['post_ratings']['placement'] = 2;

						$rating_nonrated_img = $boardurl . '/' . $modSettings['post_ratings_image_nonrated'];
						$rating_rated_img = $boardurl . '/' . $modSettings['post_ratings_image_rated'];
						$is_collapsed = empty($modSettings['post_ratings_auto_expanded']) || ($modSettings['post_ratings_auto_expanded'] == 2 && !empty($message['rating']['already_rated'])) ? true : false;

						$ratingOpt = $message['can_postratings_rate'] && empty($message['rating']['already_rated']) ? parse_bbc($modSettings['post_ratings_not_expanded_rate'], true) : parse_bbc($modSettings['post_ratings_not_expanded_edit'], true);

						// echo the right text link for them to click on
						$message['member']['custom_fields']['post_ratings']['value'] = '
										<fieldset class="postratings"' . ($is_collapsed ? ' style="display: none;"' : '') . '>
											<legend><img src="' . $settings['images_url'] . '/collapse.gif" style="vertical-align: middle;" /> ' . $ratingOpt . '</legend>
										<div>' . (!empty($message['rating']['already_rated']) ? $txt['postratings_your_rating'] : $txt['postratings_rate_it']);

						$message['member']['custom_fields']['post_ratings']['value'] .= pr_stars_input(
							$message['id'],
							isset($message['rating']['your_rating']) ? $message['rating']['your_rating'] : -1,
							$message['can_postratings_rate'] && !$message['reached_ratings_limit'] && empty($message['rating']['already_rated']) || $message['rating']['can_postratings_edit']
						);
						$message['member']['custom_fields']['post_ratings']['value'] .= '
										<span id="dRate' . $message['id'] . '" style="position: relative; left: 0px;"></span>';


						if (!empty($message['rating']['already_rated']))
						{
							$message['member']['custom_fields']['post_ratings']['value'] .= '
										<div class="floatright" style="white-space: nowrap; padding-right: 5px;">
											<a href="#"id="rate_del_' . $message['id'] . '">' . $txt['remove_rating'] . '</a>
										</div>';

						}

						$message['member']['custom_fields']['post_ratings']['value'] .= '
									</div>
										</fieldset>';
					}
				}
			}
		}
}

function pr_buffer_display($buffer)
{
	global $context;

	preg_match_all('/<div class=\\"(windowbg|windowbg2|approvebg)(.*?)(\d+)_extra_info\\">/is', $buffer, $matches, PREG_SET_ORDER);
	foreach ($matches as $match)
		$buffer = str_replace($match[0], $context['pr_head'][$match[3]] . $match[0], $buffer);

	return $buffer;
}

function pr_post_buffer($buffer)
{
	global $context, $modSettings, $txt;

	$first_disabled = empty($modSettings['post_ratings_first_post']) && $context['is_first_post'];

	$html = $context['can_ratings_enable_topic'] && $context['is_first_post'] ? '
						<li><input type="hidden" name=""ratings_topic" value="0" /><label for="check_ratings_topic"><input type="checkbox" name="ratings_topic" id="check_ratings_topic" value="1" class="input_check" ' . ($context['ratings_topic'] ? 'checked="checked" ' : '') . '/> ' . $txt['ratings_enable_topic'] . '</label></li>' : '' . '

						' . $context['can_ratings_enable_post'] && !$first_disabled ? '<li><label for="check_ratings_post"><input type="checkbox" name="nr" id="check_ratings_post"' . ($context['ratings_post'] ? '' : ' checked="checked"') . ' value="NR" class="input_check" /> ' . $txt['ratings_enable_post'] . '</label></li>' : '' . '

						' . $context['can_ratings_topic_delete'] && $context['is_first_post'] ? '<li><label for="check_ratings_delete_topic"><input type="checkbox" name="ratings_delete_topic" id="check_ratings_delete_topic" value="1" class="input_check" /> ' . $txt['ratings_delete_topic'] . '</label></li>' : '' . '

						' . $context['can_ratings_post_delete'] && !$first_disabled ? '<li><label for="check_ratings_delete_post"><input type="checkbox" name="ratings_delete_post" id="check_ratings_delete_post" value="1" class="input_check" /> ' . $txt['ratings_delete_post'] . '</label></li>' : '';

	return preg_replace('/<div id=\\"postMoreOptions\\" class=\\"smalltext\\">(.*?)<\/ul>/is', '<div id="postMoreOptions" class="smalltext">$1' . $html . '</ul>', $buffer);
}

function lite_rating($message)
{
	global $modSettings, $txt;

	$ratings_setting = unserialize($modSettings['post_ratings']);

	$message += array(
		'ratings_enabled' => !empty($message['ratings_enabled']),
		'first_enabled' => empty($modSettings['post_ratings_first_post']) && $message['id_msg'] == $topicinfo['id_first_msg'] ? false : true,
		'topic_enabled' => !empty($modSettings['post_ratings_enable']) && !empty($message['is_ratings']),
		'has_ratings' => !empty($message['total_ratings']),
		'post_rating_stars' => get_post_rating('stars', $message['id_msg'], !empty($message['total_ratings']) ? $message['total_ratings'] : 0, !empty($message['rating']) ? $message['rating'] : 0),
		'post_rating_num' => $message['rating'],
		'post_rating_totals' => !empty($message['total_ratings']) ? (int) $message['total_ratings'] : 0,
	);
	// is it enabled or not??
	if ($message['ratings_enabled'] && $message['topic_enabled'] && $message['first_enabled'] && allowedTo('postratings_view'))
	{
		$pr_body = '<table class="inner" style="border-top: none;" cellpadding="0" cellspacing="0" width="100%"><tr><td align="left" valign="top" style="white-space: nowrap;">';

		// does the post have any ratings yet?
		if (!$message['has_ratings'])
			$pr_body .= parse_bbc($modSettings['post_ratings_not_rated']);
		else
		{
			$pr_body .= $message['post_rating_stars'] . '</td>
			<td align="left" valign="middle" width="100%"><div> ' . parse_bbc($ratings_setting[$message['post_rating_num']]['text'], true) . '</div>';

			if (!empty($modSettings['post_ratings_show_quantity']))
				$pr_body .= '</td><td class="smalltext" align="right" valign="middle" style="white-space: nowrap;">' . $txt['postratings_count1'] . ' <strong>' . $message['post_rating_totals'] . '</strong> ' . ($message['post_rating_totals'] < 2 ? $txt['postratings_count2'] : $txt['postratings_count3']);
		}
		$message['body'] = $pr_body . '</td></tr></table>' . $message['body'];
	}

	return $message['body'];
}

function board_index_rating($boardIndexOptions, $categories)
{
	global $context, $options, $modSettings, $scripturl, $settings, $smcFunc, $txt, $user_info;

	if (empty($modSettings['post_ratings_enable']) || !empty($options['ratingdisable']))
		return false;

	// Get all the ratings and disratings and sort 'em out.
	$request = $smcFunc['db_query']('', '
		SELECT
			m.total_ratings,
			m.id_topic, m.id_msg, m.body, m.subject, m.poster_time, m.poster_name, m.id_member
		FROM {db_prefix}messages AS m' . ($user_info['query_see_board'] == '1=1' ? '' : '
			INNER JOIN {db_prefix}boards AS b ON (b.id_board = m.id_board AND {query_see_board})') . (!$modSettings['postmod_active'] ? '' : '
		WHERE m.approved = {int:is_approved}'),
		array(
			'is_approved' => 1,
		)
	);

	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		if (empty($row['total_ratings']))
			continue;

		$top_post[$row['total_ratings']] = array(
			'body' => $row['body'],
			'topic' => $row['id_topic'],
			'subject' => $row['subject'],
			'member' => !empty($row['id_member']) ? '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '" title="' . $txt['profile_of'] . ' ' . $row['poster_name'] . '">' . $row['poster_name'] . '</a>' : $row['poster_name'],
			'time' => timeformat($row['poster_time']),
			'id' => $row['id_msg'],
			'rating_count' => ($row['total_ratings']),
		);
	}

	if (empty($top_post))
		return false;

	krsort($top_post);
	$top_post = reset($top_post);
	$new_categories = array();

	if (!empty($modSettings['show_ratings_bi']))
		$new_board['description'] .= $top_post['rating_count'] . ' ' . $txt['ratings'] . "<br />\n\t\t\t\t\t\t";

	// Get he new board and category ready.
	$new_board = array(
		'new' => true,
		'id' => 'rating',
		'name' => $top_post['subject'],
		'description' => '<b>' . $txt['top_rated_post'] . '</b>',
		'children_new' => false,
		'posts' => -713,
		'is_redirect' => true,
		'unapproved_topics' => 0,
		'unapproved_posts' => 0,
		'can_approve_posts' => false,
		'href' => $scripturl . '?topic=' . $top_post['topic'] . '.msg' . $top_post['id'] . '#msg' . $top_post['id'],
		'link' => '<a href="' . $scripturl . '?topic=' . $top_post['topic'] . '.msg' . $top_post['id'] . '#msg' . $top_post['id'] . '">' . $top_post['subject'] . '</a>'
	);

	if (!empty($modSettings['rating_cat']))
		$new_board['description'] = '';

	if (!empty($modSettings['show_rating_post_time']))
		$new_board['description'] .= ' ' . $txt['on'] . ' ' . $top_post['time'];

	if (!empty($modSettings['show_rating_post_author']))
		$new_board['description'] .= ' ' . $txt['by'] . ' ' . $top_post['member'];

	$new_category = array(
		'id' => 'rating',
		'name' => $txt['top_rated_post'],
		'is_collapsed' => !empty($modSettings['can_rating_cat_collapse']) && !empty($options['is_rating_cat_collapsed']),
		'can_collapse' => !empty($modSettings['can_rating_cat_collapse']),
		'collapse_href' => isset($modSettings['can_rating_cat_collapse']) ? $scripturl . '?action=rating;sa=toggle;' . $context['session_var'] . '=' . $context['session_id'] : '',
		'collapse_image' => isset($modSettings['can_rating_cat_collapse']) ? '<img src="' . $settings['images_url'] . '/' . (!empty($options['is_rating_cat_collapsed']) ? 'expand.gif" alt="+"' : 'collapse.gif" alt="-"') . ' />' : '',
		'href' => $scripturl . '#category_rating',
		'link' => '<a href="' . $scripturl . '#category_rating">' . $txt['top_rated_post'] . '</a>',
		'boards' => array(),
	);

	// Hack in the average top rating/disrating.
	if ($boardIndexOptions['include_categories'])
		foreach ($categories as $id_cat => $category)
		{
			if (!empty($modSettings['rating_cat']) && $modSettings['rating_cat'] == $id_cat)
				if (empty($modSettings['rating_cat_position']))
					$new_categories['rating'] = $new_category;

			$new_categories[$id_cat] = $category;

			if (!empty($modSettings['rating_cat']) && $modSettings['rating_cat'] == $id_cat)
				if (!empty($modSettings['rating_cat_position']))
					$new_categories['rating'] = $new_category;

			if (empty($modSettings['rating_cat']))
			{
				$new_boards = array();
				foreach ($category['boards'] as $id_board => $board)
				{
					if (!empty($modSettings['rating_board']) && $modSettings['rating_board'] == $id_board)
						if (empty($modSettings['rating_board_position']))
							$new_boards['rating'] = $new_board;

					$new_boards[$id_board] = $board;

					if (!empty($modSettings['rating_board']) && $modSettings['rating_board'] == $id_board)
						if (!empty($modSettings['rating_board_position']))
							$new_boards['rating'] = $new_board;
				}
				$new_categories[$id_cat]['boards'] = $new_boards;
			}
			else
				$new_categories['rating']['boards'][] = $new_board;
		}

	// Modify the buffer to clean up.
	add_integration_function('integrate_buffer', 'integrate_board_index_rating_buffer', false);

	if (!empty($new_categories))
		return $new_categories;
	else
		return false;
}

function integrate_board_index_rating_buffer($buffer)
{
	global $context, $txt;

	return str_replace(comma_format(-713) . ' ' . $txt['redirects'], $context['rating_count'], $buffer);
}

function compare_top_ratings($a, $b)
{
    return $a['num_ratings'] < $b['num_ratings'] ? 1 : -1;
}

function statsRating()
{
	global $context, $modSettings, $options, $scripturl, $smcFunc, $txt;

	if (empty($modSettings['post_ratings_enable']) || !empty($options['ratedisable']))
		return false;

	// Top ten rated posts
	$request = $smcFunc['db_query']('', '
		SELECT m.subject, m.id_topic, m.id_msg, m.rating
		FROM {db_prefix}messages AS m
			INNER JOIN {db_prefix}boards AS b ON (b.id_board = m.id_board' . (!empty($modSettings['recycle_enable']) && $modSettings['recycle_board'] > 0 ? '
			AND b.id_board != {int:recycle_board}' : '') . ')
		WHERE {query_see_board}' . ($modSettings['postmod_active'] ? '
			AND m.approved = {int:is_approved}' : '') . '
			AND m.ratings_enabled = {int:ratings_enabled}
			AND rating != 0
		ORDER BY rating DESC
		LIMIT 10',
		array(
			'recycle_board' => $modSettings['recycle_board'],
			'is_approved' => 1,
			'ratings_enabled' => 1,
		)
	);
	$context['top_topics_ratings'] = array();
	$max_num_ratings = 1;
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		if ($row['rating'] == 0)
			continue;

		censorText($row['subject']);

		$context['top_topics_ratings'][] = array(
			'num_ratings' => $row['rating'],
			'link' => '<a href="' . $scripturl . '?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . '#msg' . $row['id_msg'] . '">' . $row['subject'] . '</a>'
		);

		if ($max_num_ratings < $row['rating'])
			$max_num_ratings = $row['rating'];
	}
	$smcFunc['db_free_result']($request);

	usort($context['top_topics_ratings'], 'compare_top_ratings');
	foreach ($context['top_topics_ratings'] as $i => $topic)
	{
		$context['top_topics_ratings'][$i]['post_percent'] = round(($topic['num_ratings'] * 100) / $max_num_ratings);
		$context['top_topics_ratings'][$i]['num_ratings'] = comma_format($context['top_topics_ratings'][$i]['num_ratings']);
	}

	// Top ten members who rating posts
	$request = $smcFunc['db_query']('', '
		SELECT id_member, COUNT(id_msg) AS num
		FROM {db_prefix}log_message_ratings
		GROUP BY id_member
		LIMIT 10');
	$top_raters = array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$top_raters[$row['id_member']] = $row['num'];
	$smcFunc['db_free_result']($request);

	$request = $smcFunc['db_query']('', '
		SELECT mem.id_member, mem.real_name
		FROM {db_prefix}members AS mem
		WHERE mem.id_member IN ({array_int:loaded_ids})',
		array(
			'loaded_ids' => array_keys($top_raters),
		)
	);
	$context['top_raters'] = array();
	$max_num_ratings = 1;
	while ($row = $smcFunc['db_fetch_assoc']($request))
	{
		$context['top_raters'][] = array(
			'num_ratings' => $top_raters[$row['id_member']],
			'link' => '<a href="' . $scripturl . '?action=profile;u=' . $row['id_member'] . '">' . $row['real_name'] . '</a>'
		);

		if ($max_num_ratings < $top_raters[$row['id_member']])
			$max_num_ratings = $top_raters[$row['id_member']];
	}
	$smcFunc['db_free_result']($request);

	usort($context['top_raters'], 'compare_top_ratings');
	foreach ($context['top_raters'] as $i => $topic)
	{
		$context['top_raters'][$i]['post_percent'] = round(($topic['num_ratings'] * 100) / $max_num_ratings);
		$context['top_raters'][$i]['num_ratings'] = comma_format($context['top_raters'][$i]['num_ratings']);
	}

	// Modify the buffer to add our stats.
	add_integration_function('integrate_buffer', 'integrate_stats_rating_buffer', false);

	$context['html_headers'] .= '
	<style type="text/css">
		.halfwidth
		{
			width: 49.5%;
		}
	</style>';
}

function template_stats_block_ratings($stats, $title, $image)
{
	$html = '
				<div class="title_bar">
					<h3 class="titlebg">
						<img src="' . $image . '" class="icon" alt="" /> ' . $title . '
					</h3>
				</div>
				<div class="windowbg2">
					<span class="topslice"><span></span></span>
					<div class="content">
						<dl class="stats">';

	foreach ($stats as $poster)
	{
		$html .= '
							<dt>
								' . $poster['link'] . '
							</dt>
							<dd class="statsbar">';

		if (!empty($poster['post_percent']))
			$html .= '
								<div class="bar" style="width: ' . ($poster['post_percent'] + 4) . 'px;">
									<div style="width: ' . $poster['post_percent'] . 'px;"></div>
								</div>';

		$html .= '
								<span>' . $poster['num_ratings'] . '</span>
							</dd>';
	}

	$html .= '
						</dl>
						<div class="clear"></div>
					</div>
					<span class="botslice"><span></span></span>
				</div>';

	return $html;
}

function integrate_stats_rating_buffer($buffer)
{
	global $context, $modSettings, $settings, $txt;
	$i = 0;
	$html = '';
	$alternate = true;
	$block_settings = array(
		'show_top_ten_ratings' => array(
			'stats' => $context['top_topics_ratings'],
			'image' => $settings['images_url'] . '/stats_replies.gif',
			'text' => $txt['top_ten_ratings'],
		),
		'show_top_ten_raters' => array(
			'stats' => $context['top_raters'],
			'image' => $settings['images_url'] . '/stats_posters.gif',
			'text' => $txt['top_ten_raters'],
		),
	);

	foreach ($block_settings as $block_setting => $block_data)
	{
		if (empty($modSettings[$block_setting]))
			continue;

		$html .= '
		<div class="float'  . ($alternate ? 'left' : 'right') . ' halfwidth">' . template_stats_block_ratings($block_data['stats'], $block_data['text'], $block_data['image']) . '</div>';

		$alternate = !$alternate;
	}

	return preg_replace('/' . preg_quote($txt['most_time_online']) . '(.+)<br class=\\"clear\\" \/>/is', $txt['most_time_online'] . '$1' . $html . '<br class="clear" />', $buffer);
}

function loadProfileFieldsRating()
{
	global $cur_profile, $modSettings, $txt;

	// if (empty($modSettings['post_ratings_enable']))
		// return false;

	$fields = array(
		'ratedisable' => array(
			'type' => 'check',
			'name' => $txt['rating_disable'],
			'desc' => '',
			'colname' =>'ratedisable',
			'input_html' => '<input type="checkbox" name="customfield[ratedisable]" ' . (!empty($cur_profile['options']['ratedisable']) ? 'checked="checked"' : '') . ' class="input_check" />',
		),
	);

	return $fields;
}

function save_custom_profile_fields_rating(&$changes, &$log_changes, $memID, $area, $sanitize)
{
	global $cur_profile, $modSettings, $user_info;

	// if (empty($modSettings['post_ratings_enable']))
		// return false;

	$fields = loadProfileFieldsRating();

	foreach ($fields as $field)
	{
		switch ($field['type'])
		{
			case 'check';
				$value = isset($_POST['customfield'][$field['colname']]) ? 1 : 0;
				break;
		}

		// Did it change?
		if (!isset($cur_profile['options'][$field['colname']]) || $cur_profile['options'][$field['colname']] != $value)
		{
			$log_changes[] = array(
				'action' => $field['colname'],
				'id_log' => 2,
				'log_time' => time(),
				'id_member' => $memID,
				'ip' => $user_info['ip'],
				'extra' => serialize(array('previous' => !empty($cur_profile['options'][$field['colname']]) ? $cur_profile['options'][$field['colname']] : '', 'new' => $value, 'applicator' => $user_info['id'])),
			);
			$changes[] = array(1, $field['colname'], $value, $memID);
		}
	}
}

?>