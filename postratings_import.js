function draftAutosave(oOptions)
{
	this.opt = oOptions;
	addLoadEvent(this.opt.sSelf + '.init();');
}

draftAutosave.prototype.init = function init()
{
	this.timeout = setInterval(this.opt.sSelf + '.draftSend();', 500);
	this.timer = 0;
}

draftAutosave.prototype.draftSend = function ()
{
	var x = [
		'step=' + this.opt.iStep,
		'count=' + this.opt.iCount,
		this.opt.sSessionVar + '=' + this.opt.sSessionId
	];

	var sUrl = smf_prepareScriptUrl(this.opt.sScriptUrl) + "action=admin;area=postratings;sa=i;do;xml";

	// Send in the XMLhttp request and let's hope for the best.
	sendXMLDocument.call(this, sUrl, x.join("&"), this.draftReply);

	return false;
}

draftAutosave.prototype.draftReply = function (XMLDoc)
{
	this.timer += 500;
	var oDiv = document.getElementsByTagName('progress')[0], oNote = XMLDoc.getElementsByTagName('lastsave')[0],
	v = oNote.getAttribute('value'), max = oNote.getAttribute('max'), p = Math.round((v / max) * 100),
	s = Math.round((this.timer) / 1000), t = Math.round((s / v) * (max - v));
	oDiv.value = v;
	oDiv.max = max;
	this.opt.iStep = v;
	this.opt.iCount = max;
	document.getElementById('stats').innerHTML = 'Step 1 of 2: Copying ratings...<br>' + v + ' / ' + max + ' (' + p + '%)<br>Estimated time remaining: about ' + t + ' second(s) (' + s + ' elapsed)';

	if (p >= 100)
	{
		clearTimeout(this.timeout);
		document.getElementById('stats').innerHTML += '<br><br><div id="stats2">...</div>';
		this.timeout = setInterval(this.opt.sSelf + '.draftSend2();', 500);
		this.timer = 0;
		this.opt.iStep = 1;
	}
}

draftAutosave.prototype.draftSend2 = function ()
{
	var x = [
		'step=' + this.opt.iStep,
		'count=' + this.opt.iCount,
		this.opt.sSessionVar + '=' + this.opt.sSessionId
	];

	var sUrl = smf_prepareScriptUrl(this.opt.sScriptUrl) + "action=admin;area=postratings;sa=i;do;xml;msg";

	// Send in the XMLhttp request and let's hope for the best.
	sendXMLDocument.call(this, sUrl, x.join("&"), this.draftReply2);

	return false;
}

draftAutosave.prototype.draftReply2 = function (XMLDoc)
{
	this.timer += 500;
	var oDiv = document.getElementsByTagName('progress')[0], oNote = XMLDoc.getElementsByTagName('lastsave')[0],
	v = oNote.getAttribute('value'), max = oNote.getAttribute('max'), p = Math.round((v / max) * 100),
	s = Math.round((this.timer) / 1000), t = Math.round((s / v) * (max - v));
	oDiv.value = v;
	oDiv.max = max;
	this.opt.iStep = v;
	this.opt.iCount = max;
	document.getElementById('stats2').innerHTML = 'Step 2 of 2: Convering rated messages...<br>' + v + ' / ' + max + ' (' + p + '%)<br>Estimated time remaining: about ' + t + ' second(s) (' + s + ' elapsed)';

	if (p >= 100)
	{
		clearTimeout(this.timeout);
		document.getElementById('msg').innerHTML = 'Done.';
	}
}
