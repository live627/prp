<?php

/**
 * Post Ratings Pro
 *
 */

if (!defined('SMF'))
	die('Hacking attempt...');

// This function passes control through to the relevant tab.
function ManagePostRatings()
{
	global $context, $txt, $scripturl, $modSettings, $settings, $sourcedir;

	// You need to be an admin to edit settings!
	isAllowedTo('admin_forum');

	loadLanguage('Help');
	loadLanguage('ManageSettings');

	// Will need the utility functions from here.
	require_once($sourcedir . '/ManageServer.php');

	$context['sub_template'] = 'show_settings';
	$context['page_title'] = $txt['post_ratings_settings'];

	$sub_actions = array(
		'general' => 'ModifyPostRatingsGeneral',
		'boards' => 'ModifyPostRatingsBoards',
		'i' => 'ModifyPostRatingsI',
	);

	// Default to sub action 'general'
	if (!isset($_GET['sa']) || !isset($sub_actions[$_GET['sa']]))
		$_GET['sa'] = 'general';

	// Load up all the tabs...
	$context[$context['admin_menu_name']]['tab_data'] = array(
		'title' => $txt['post_ratings_settings'],
		'help' => 'featuresettings',
		'description' => $txt['post_ratings_settings_desc'],
	);

	// Call the right function for this sub-acton.
	$sub_actions[$_GET['sa']]();
}

function ModifyPostRatingsI($return_config = false)
{
	global $txt, $scripturl, $context, $settings, $modSettings, $sourcedir, $smcFunc, $boardurl;

	$request = $smcFunc['db_query']('', '
		SELECT COUNT(*)
		FROM {db_prefix}message_ratings');
	list ($context['row_count']) = $smcFunc['db_fetch_row']($request);

	if (isset($_GET['do']))
	{
		if (isset($_GET['xml']))
		{
			if (isset($_GET['msg']))
			{
				$request = $smcFunc['db_query']('', '
					SELECT COUNT(*)
					FROM {db_prefix}messages
					WHERE rating > 0');
				list ($context['row_count2']) = $smcFunc['db_fetch_row']($request);
				$next_step2 = $_POST['step'] + 147;
				if ($next_step2 > $context['row_count2'])
					$next_step2 = $context['row_count2'];
				$request = $smcFunc['db_query']('', '
					SELECT
						AVG(value), id_msg
					FROM {db_prefix}log_message_ratings
					WHERE id_msg BETWEEN {int:this_step} AND {int:next_step}',
					array(
						'this_step' => $_POST['step'],
						'next_step' => $next_step2,
					)
				);
				$updates = array();
				while ($row = $smcFunc['db_fetch_row']($request))
					$smcFunc['db_query']('', '
						UPDATE {db_prefix}messages
						SET rating = {raw:avg}
						WHERE id_msg = {int:id_msg}',
						array(
							'id_msg' => $row[1],
							'avg' => $row[0],
						)
					);

				header('Content-Type: text/xml; charset=' . (empty($context['character_set']) ? 'ISO-8859-1' : $context['character_set']));
				echo '<', '?xml version="1.0" encoding="', $context['character_set'], '"?', '>
				<response>
					<lastsave value="', $next_step2, '" max="', $context['row_count2'], '" />
				</response>';

				obExit(false);
			}

			$next_step = $_POST['step'] + 244;
			if ($next_step > $context['row_count'])
				$next_step = $context['row_count'];
			$request = $smcFunc['db_query']('', '
				SELECT *
				FROM {db_prefix}message_ratings
				WHERE id_rating BETWEEN {int:this_step} AND {int:next_step}',
				array(
					'this_step' => $_POST['step'],
					'next_step' => $next_step,
				)
			);
			$inserts = array();
			while ($row = $smcFunc['db_fetch_assoc']($request))
				$inserts += array(
					$row['id_rating'],
					$row['id_msg'],
					$row['id_member'],
					$row['date'],
					$row['value'] - 1,
				);

			$smcFunc['db_insert']('',
				'{db_prefix}log_message_ratings',
				array(
					'id_rating' => 'int', 'id_msg' => 'int', 'id_member' => 'int', 'date' => 'int', 'value' => 'int'
				),
				array(
					$inserts,
				),
				array('id_rating', 'id_msg')
			);

			header('Content-Type: text/xml; charset=' . (empty($context['character_set']) ? 'ISO-8859-1' : $context['character_set']));
			echo '<', '?xml version="1.0" encoding="', $context['character_set'], '"?', '>
			<response>
				<lastsave value="', $next_step, '" max="', $context['row_count'], '" />
			</response>';

			obExit(false);
		}
		else
			$request = $smcFunc['db_query']('', '
				TRUNCATE TABLE {db_prefix}log_message_ratings');

		$context['html_headers'] .= '
		<script type="text/javascript" src="' . $settings['default_theme_url'] . '/scripts/postratings_import.js?rc3"></script>';
		$context['sub_template'] = 'i_do';
	}
	else
		$context['sub_template'] = 'i';
}

function template_i()
{
	global $context, $txt, $scripturl;

	echo '
	<div class="cat_bar"><h3 class="catbg">Import</h3></div>
	<div class="windowbg">
		<span class="topslice"><span></span></span>
		<div class="padding">
			<b>This will import data from the free edition of Post Ratings.</b>
			<hr />
			<a href="', $scripturl, '?action=admin;area=postratings;sa=i;do">Import data now!</a>
		</div>
		<span class="botslice"><span></span></span>
	</div>';
}

function template_i_do()
{
	global $context, $txt, $scripturl;

	echo '
	<div class="cat_bar"><h3 class="catbg">Import</h3></div>
	<div class="windowbg">
		<span class="topslice"><span></span></span>
		<div class="padding">
			<b>Now importing data from the free edition of Post Ratings.</b>
			<hr />
			<div id="msg">Please wait for the process to complete. The progress bar is updated from the server in real time.></div><br /><br />
			<progress></progress><br /><br /><div id="stats"></div>
		</div>
		<span class="botslice"><span></span></span>
	</div>
		<script type="text/javascript"><!-- // --><![CD', 'ATA[
			var oAutoSave = new draftAutosave({
				sSelf: \'oAutoSave\',
				sScriptUrl: smf_scripturl,
				iStep: 1,
				iCount: ', $context['row_count'], ',
				sSessionId: \'', $context['session_id'], '\',
				sSessionVar: \'', $context['session_var'], '\'
			});
		// ]', ']></script>';
}

function ModifyPostRatingsGeneral($return_config = false)
{
	global $txt, $scripturl, $context, $settings, $modSettings, $sourcedir, $smcFunc, $boardurl;

	$request = $smcFunc['db_query']('order_by_cat_order', '
		SELECT id_cat, name AS cat_name
		FROM {db_prefix}categories');
	$categories = array($txt['post_ratings_category_all']);
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$categories[$row['id_cat']] = $row['cat_name'];

	$request = $smcFunc['db_query']('order_by_board_order', '
		SELECT b.id_board, b.name AS board_name, c.name AS cat_name
		FROM {db_prefix}boards AS b
			LEFT JOIN {db_prefix}categories AS c ON (c.id_cat = b.id_cat)
		WHERE redirect = {string:empty_string}',
		array(
			'empty_string' => '',
		)
	);
	$boards = array($txt['post_ratings_board_none']);
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$boards[$row['id_board']] = $row['cat_name'] . ' - ' . $row['board_name'];

	$config_vars = array(
		array('title', 'post_ratings_forum_integration'),
		array('desc', 'post_ratings_forum_integration_desc'),
		array('check', 'show_rating_profile'),
		array('check', 'show_rating_recent'),
		array('check', 'show_rating_search'),
		array('check', 'show_top_ten_ratings'),
		array('check', 'show_top_ten_raters'),
		array('title', 'post_ratings_bi_integration'),
		array('select', 'rating_cat_position', array($txt['before'], $txt['after'])),
		array('select', 'rating_cat', $categories, 'subtext' => $txt['rating_cat_desc']),
		array('check', 'can_rating_cat_collapse'),
		array('select', 'rating_board_position', array($txt['before'], $txt['after'])),
		array('select', 'rating_board', $boards, 'subtext' => $txt['rating_board_desc']),
		array('check', 'show_rating_post_time'),
		array('check', 'show_rating_post_author'),
		array('check', 'show_rating_bi'),
	);

	if ($return_config)
		return $config_vars;

	// Saving?
	if (isset($_GET['save']))
	{
		checkSession();

		saveDBSettings($config_vars);

		writeLog();
		redirectexit('action=admin;area=postratings;sa=general');
	}

	$context['post_url'] = $scripturl . '?action=admin;area=postratings;save;sa=general';

	prepareDBSettingContext($config_vars);
}

function ModifyPostRatingsBoards($return_config = false)
{
	global $txt, $scripturl, $context, $settings, $modSettings, $board, $smcFunc, $boardurl;

	$request = $smcFunc['db_query']('order_by_board_order', '
		SELECT b.id_board, b.name AS board_name, c.name AS cat_name
		FROM {db_prefix}boards AS b
			LEFT JOIN {db_prefix}categories AS c ON (c.id_cat = b.id_cat)
		WHERE redirect = {string:empty_string}',
		array(
			'empty_string' => '',
		)
	);
	$boards = array($txt['post_ratings_board_none']);
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$boards[$row['id_board']] = $row['cat_name'] . ' - ' . $row['board_name'];

	$config_vars = array(
			array('callback', 'board_select'),
			array('title', 'post_ratings_settings_general'),
			array('check', 'post_ratings_enable'),
			array('select', 'post_ratings_autoenable_topic_mode', array(&$txt['post_ratings_autoenable_disabled_new'], &$txt['post_ratings_autoenable_new_topics'], &$txt['post_ratings_autoenable_disabled_all'], &$txt['post_ratings_autoenable_all_topics']), 'help' => 'post_ratings_autoenable_topic_mode_help'),
			array('check', 'post_ratings_first_post', 'help' => 'post_ratings_first_post_help'),
			array('check', 'post_ratings_topic_only', 'subtext' => $txt['post_ratings_topic_only_desc']),
			array('check', 'post_ratings_mi', 'help' => 'post_ratings_mi_help'),
			array('check', 'post_ratings_disable_locked_topic', 'help' => 'post_ratings_disable_locked_topic_help'),
			array('int', 'post_ratings_limit_count', 'help' => 'post_ratings_limit_count_help', 'size' => 2),
		'',
			array('select', 'post_ratings_layout_style', array(&$txt['post_ratings_layout_style_inner'], &$txt['post_ratings_layout_style_header']), 'help' => 'post_ratings_layout_style_help', 'javascript' => 'onChange="prChange(this);"', 'postinput' => '<img src="" alt="" style="display: block;" id="prImage" />'),
			array('select', 'post_ratings_auto_expanded', array(&$txt['post_ratings_auto_expanded_disabled'], &$txt['post_ratings_auto_expanded_enabled'], &$txt['post_ratings_auto_expanded_enabled_new']), 'help' => 'post_ratings_auto_expanded_help'),
			array('check', 'post_ratings_show_quantity', 'help' => 'post_ratings_show_quantity_help'),
			array('check', 'post_ratings_show_lastratedtime', 'help' => 'post_ratings_show_lastratedtime_help'),
			array('check', 'post_ratings_posts_enable_icon', 'help' => 'post_ratings_posts_enable_icon_help'),
			array('check', 'post_ratings_posts_delete_icon', 'help' => 'post_ratings_posts_delete_icon_help'),
		array('title', 'post_ratings_images', 'help' => 'post_ratings_images_help'),
			array('text', 'post_ratings_image_nonrated', 'size' => 40,  'postinput' => '<img src="' . $boardurl . '/' . $modSettings['post_ratings_image_nonrated'] . '" border="0" style="position: relative; top: 3px;" />'),
			array('text', 'post_ratings_image_rated', 'size' => 40,  'postinput' => '<img src="' . $boardurl . '/' . $modSettings['post_ratings_image_rated'] . '" border="0" style="position: relative; top: 3px;" />'),
		array('title', 'post_ratings_text', 'help' => 'post_ratings_text_help'),
		array('desc', 'post_ratings_text_desc'),
			array('text', 'post_ratings_not_expanded_rate', 'help' => 'post_ratings_not_expanded_rate_help', 'size' => 40),
			array('text', 'post_ratings_not_expanded_edit', 'help' => 'post_ratings_not_expanded_edit_help', 'size' => 40),
		'',
			array('text', 'post_ratings_not_rated', 'size' => 40),
		'',
			array('callback', 'post_ratings'),
	);

	if ($return_config)
		return $config_vars;

	$board = !empty($_REQUEST['board']) ? (int) $_REQUEST['board'] : 1;
	$request = $smcFunc['db_query']('', '
		SELECT variable, value
		FROM {db_prefix}ratings_settings
		WHERE id_board = {int:board}',
		array(
			'board' => $board,
		)
	);
	while ($row = $smcFunc['db_fetch_row']($request))
		$modSettings[$row[0]] = $row[1];
	$smcFunc['db_free_result']($request);

	// Saving?
	if (isset($_GET['save']) && !isset($_POST['chbrd']))
	{
		checkSession();

		// Topic Enabled or Disabled and New or All
		if (!empty($_POST['post_ratings_autoenable_topic_mode']) && ($_POST['post_ratings_autoenable_topic_mode'] != '1'))
		{
			// All Topics - Enable or Disable them?!
			$enable = $_POST['post_ratings_autoenable_topic_mode'] == '3' ? 1 : 0;

			// this could take awhile...

			db_extend('packages');
			// change the default to 1 (Auto Enabled)
			$smcFunc['db_change_column']('{db_prefix}topics', 'is_ratings', array('default' => $enable));
		}
		else
		{
			// New Topics - Enable or Disable them by default?!
			$enable = $_POST['post_ratings_autoenable_topic_mode'] == '1' ? 1 : 0;

			// New Topics Only!
			db_extend('packages');

			// change the default pending
			$smcFunc['db_change_column']('{db_prefix}topics', 'is_ratings', array('default' => $enable));
		}

		// The crux of Post Ratings!!
		$ratings = array();
		foreach ($_POST['ratings'] as $k => $v)
			$ratings[$k] = $v;
		pr_updateSettings(array('post_ratings' => serialize($ratings)), $board);

		pr_saveDBSettings($config_vars, $board);

		writeLog();
		redirectexit('action=admin;area=postratings;sa=boards;board=' . $board);
	}

	$context['post_url'] = $scripturl . '?action=admin;area=postratings;save;sa=boards;board=' . $board;
	$context['settings_title'] = $txt['post_ratings_settings_boards'];
	$context['settings_post_javascript'] = '
	var imgpath = ' . JavaScriptEscape($settings['default_images_url'] . '/post_ratings') . ';
	prChange(document.getElementById("post_ratings_layout_style"));

	function prChange(obj)
	{
		if (obj.value)
			document.getElementById("prImage").src = imgpath + "/inner.png";
		else
			document.getElementById("prImage").src = imgpath + "/header.png";
	}

	function addRow(element)
	{
		count++;
		myRow = document.createElement("tr");

		myCell = myRow.insertCell(-1);
		myCell.innerHTML = ' . JavaScriptEscape($txt['post_ratings_number'] . ' ') . ' + count;

		myCell = myRow.insertCell(-1);
		myCell.className = "centertext";
		var myInput = document.createElement("input");
		myInput.type = "text";
		myInput.value = "text";
		myInput.name = "ratings[][text]";
		myInput.size = 40;
		myInput.maxlength = 40;
		myCell.appendChild(myInput);

		element.parentNode.parentNode.parentNode.insertBefore(myRow, element.parentNode.parentNode);
		return false;
	}';

	prepareDBSettingContext($config_vars);
}

function template_callback_post_ratings()
{
	global $context, $modSettings, $board, $settings, $txt;

	if (isset($board, $modSettings['post_ratings']))
		$ratings_setting = unserialize($modSettings['post_ratings']);
	else
		$ratings_setting = array(
			array(
				'threshold' => '20',
				'text' => 'Poor',
			),
			array(
				'threshold' => '50',
				'text' => 'Fair',
			),
			array(
				'threshold' => '75',
				'text' => 'Good',
			),
			array(
				'threshold' => '90',
				'text' => 'Great',
			),
			array(
				'threshold' => '100',
				'text' => 'Excellent',
			),
		);

	echo '
					</dl>
			<table width="100%">
				<thead>
				<tr>
					<th></th>
					<th>', $txt['post_ratings'], '</th>
				</tr>
				</thead>';

	foreach ($ratings_setting as $id => $rating)
		echo '
				<tr>
					<td>', $txt['post_ratings_number'], ' ' , $id + 1,  '</td>
					<td class="centertext"><input type="text" value="', $rating['text'], '" name="ratings[][text]" size="40" maxlength="40" /></td>
				</tr>';

	echo '
				<tr id="pr_more">
					<td colspan="3" class="righttext padding">
						<a href="#" onclick="return addRow(this);">(', $txt['more'], ')</a>
					</td>
				</tr>
			</table>
					<dl class="settings">';

	$context['settings_post_javascript'] .= '
	var count = ' . count($ratings_setting);
}

function template_callback_board_select()
{
	global $txt, $context, $scripturl, $smcFunc, $board;

	$request = $smcFunc['db_query']('order_by_board_order', '
		SELECT b.id_board, b.name AS board_name, c.name AS cat_name
		FROM {db_prefix}boards AS b
			LEFT JOIN {db_prefix}categories AS c ON (c.id_cat = b.id_cat)
		WHERE redirect = {string:empty_string}',
		array(
			'empty_string' => '',
		)
	);
	$boards = array();
	while ($row = $smcFunc['db_fetch_assoc']($request))
		$boards[$row['id_board']] = $row['cat_name'] . ' - ' . $row['board_name'];

	echo '
					<dt>Select a board to modify</dt>
					<dd>
						<select name="board">';

	foreach ($boards as $id_board => $name)
		echo  '
							<option value="', $id_board, '"', $id_board == $board ? ' selected="selected"' : '', '>', $name, '</option>';

	echo '
						</select>
						<input type="submit" name="chbrd" value="Go" />
					</dd>';
}

function pr_saveDBSettings(&$config_vars, $id_board)
{
	global $sourcedir, $context;

	foreach ($config_vars as $var)
	{
		if (!isset($var[1]) || (!isset($_POST[$var[1]]) && $var[0] != 'check' && $var[0] != 'permissions' && ($var[0] != 'bbc' || !isset($_POST[$var[1] . '_enabledTags']))))
			continue;

		// Checkboxes!
		elseif ($var[0] == 'check')
			$set_array[$var[1]] = !empty($_POST[$var[1]]) ? '1' : '0';
		// Select boxes!
		elseif ($var[0] == 'select' && in_array($_POST[$var[1]], array_keys($var[2])))
			$set_array[$var[1]] = $_POST[$var[1]];
		elseif ($var[0] == 'select' && !empty($var['multiple']) && array_intersect($_POST[$var[1]], array_keys($var[2])) != array())
		{
			// For security purposes we validate this line by line.
			$options = array();
			foreach ($_POST[$var[1]] as $invar)
				if (in_array($invar, array_keys($var[2])))
					$options[] = $invar;

			$set_array[$var[1]] = serialize($options);
		}
		// Integers!
		elseif ($var[0] == 'int')
			$set_array[$var[1]] = (int) $_POST[$var[1]];
		// Floating point!
		elseif ($var[0] == 'float')
			$set_array[$var[1]] = (float) $_POST[$var[1]];
		// Text!
		elseif ($var[0] == 'text' || $var[0] == 'large_text')
			$set_array[$var[1]] = $_POST[$var[1]];
		// Passwords!
		elseif ($var[0] == 'password')
		{
			if (isset($_POST[$var[1]][1]) && $_POST[$var[1]][0] == $_POST[$var[1]][1])
				$set_array[$var[1]] = $_POST[$var[1]][0];
		}
		// BBC.
		elseif ($var[0] == 'bbc')
		{
			$bbcTags = array();
			foreach (parse_bbc(false) as $tag)
				$bbcTags[] = $tag['tag'];

			if (!isset($_POST[$var[1] . '_enabledTags']))
				$_POST[$var[1] . '_enabledTags'] = array();
			elseif (!is_array($_POST[$var[1] . '_enabledTags']))
				$_POST[$var[1] . '_enabledTags'] = array($_POST[$var[1] . '_enabledTags']);

			$set_array[$var[1]] = implode(',', array_diff($bbcTags, $_POST[$var[1] . '_enabledTags']));
		}
	}

	if (!empty($set_array))
		pr_updateSettings($set_array, $id_board);
}

function pr_updateSettings($change_array, $id_board)
{
	global $modSettings, $smcFunc;

	if (empty($change_array) || !is_array($change_array))
		return;

	$replace_array = array();
	foreach ($change_array as $variable => $value)
	{
		// Don't bother if it's already like that ;).
		if (isset($modSettings[$variable]) && $modSettings[$variable] == $value)
			continue;
		// If the variable isn't set, but would only be set to nothing'ness, then don't bother setting it.
		elseif (!isset($modSettings[$variable]) && empty($value))
			continue;

		$replace_array[] = array($id_board, $variable, $value);

		$modSettings[$variable] = $value;
	}

	if (empty($replace_array))
		return;

	$smcFunc['db_insert']('replace',
		'{db_prefix}ratings_settings',
		array('id_board' => 'int', 'variable' => 'string-255', 'value' => 'string-65534'),
		$replace_array,
		array('variable')
	);
}

?>