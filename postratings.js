function postRatings(oOptions)
{
	this.opt = oOptions;
	self = this;

	var fieldsets = document.getElementsByTagName('fieldset');
	for (var f = 0; f < fieldsets.length; f++)
	{
		if (fieldsets[f].className == 'postratings')
		{
			var
				n = fieldsets[f].getElementsByTagName('legend')[0],
				msg = fieldsets[f].id.replace('msg_', ''),
				els = fieldsets[f].getElementsByTagName('img'),
				l = els.length;
			createEventListener(n);
			n.addEventListener('click', function()
			{
				this.parentNode.style.display = 'none';
				this.parentNode.previousSibling.style.display = '';
			}, false);
			n.style.cursor = 'pointer';
			var t = document.createElement("a");
			t.href = 'javascript:void(0);';
			t.style.display = fieldsets[f].style.display == 'none' ?  '' : 'none';
			t.innerHTML = n.innerHTML;
			var s = t.getElementsByTagName('img')[0];
			s.src = s.src.replace('collapse', 'expand');
			createEventListener(t);
			t.addEventListener('click', function()
			{
				this.nextSibling.style.display = '';
				this.style.display = 'none';
			}, false);
			fieldsets[f].parentNode.insertBefore(t, fieldsets[f]);
			for (i = 0; i < l; i++)
			{
				oImg = els[i];
				if (oImg.className == 'pr')
				{
					createEventListener(oImg);
					oImg.addEventListener('click', function()
					{
						self.sendRating(this.id.replace('rating_', '').split('_'));
					}, false);
					oImg.addEventListener('mouseover', function()
					{
						self.doMover(this.id.replace('rating_', '').split('_'));
					}, false);
					oImg.addEventListener('mouseout', function()
					{
						self.doMout(this.id.replace('rating_', '').split('_'));
					}, false);
					oImg.style.cursor = 'pointer';
					rating = oImg.id.replace('rating_', '').split('_');
					elDel = document.getElementById('rate_del_' + rating[1]);
					if (rating[0] == 0)
					{
						this.textOutput([this.opt.oStars[rating[1]], rating[1]]);
						if (elDel)
						{
							elDel.href = 'javascript:void(0);';
							createEventListener(elDel);
							elDel.addEventListener('click', function()
							{
								self.sendRating(this.id.replace('rate_', '').split('_'));
							}, false);
						}
					}
				}
			}
		}
	}
}

postRatings.prototype.doMover = function (rating)
{
	var l = this.opt.rateText.length;
	for (i = 0; i < l; i++)
	{
		if (i <= rating[0])
			document.getElementById('rating_' + i + '_' + rating[1]).src = this.opt.imageOver;
		else
			document.getElementById('rating_' + i + '_' + rating[1]).src = this.opt.imageOut;
	}
	this.textOutput(rating);
}

postRatings.prototype.doMout = function (rating)
{
	var l = this.opt.rateText.length;
	for (i = 0; i < l; i++)
		if (i <= this.opt.oStars[rating[1]])
			document.getElementById('rating_' + i + '_' + rating[1]).src = this.opt.imageOver;
		else
			document.getElementById('rating_' + i + '_' + rating[1]).src = this.opt.imageOut;
	this.textOutput([this.opt.oStars[rating[1]], rating[1]]);
}

postRatings.prototype.textOutput = function (rating)
{
	if (rating[0] == -1)
		setInnerHTML(document.getElementById('dRate' + rating[1]), '');
	else
		setInnerHTML(document.getElementById('dRate' + rating[1]), this.opt.rateText[rating[0]]);
}

postRatings.prototype.sendRating = function (rating)
{
	var x = [
		'rating=' + rating[0],
		'msg=' + rating[1],
		'topic=' + this.opt.iTopic,
		this.opt.sSessionVar + '=' + this.opt.sSessionId
	];

	if (rating[0] == 'del')
		sa = 'delete';
	else
		sa = 'rate';

	var sUrl = smf_prepareScriptUrl(smf_scripturl) + 'action=postratings;sa=' + sa + ';xml';

	// Send in the XMLhttp request and let's hope for the best.
	sendXMLDocument.call(this, sUrl, x.join('&'), this.callback);

	return false;
}

postRatings.prototype.callback = function (XMLDoc)
{
	var oNote = XMLDoc.getElementsByTagName('rating')[0];
	this.opt.oStars[oNote.getAttribute('msg')] = oNote.getAttribute('stars');
	this.doMout([oNote.getAttribute('stars'), oNote.getAttribute('msg')]);
	this.textOutput([oNote.getAttribute('stars'), oNote.getAttribute('msg')]);
}