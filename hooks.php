<?php

if (file_exists(dirname(__FILE__) . '/SSI.php') && !defined('SMF'))
{
	$ssi = true;
	require_once(dirname(__FILE__) . '/SSI.php');
}
elseif (!defined('SMF'))
	exit('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');

add_integration_function('integrate_pre_include', '$sourcedir/Subs-PostRatings.php');
add_integration_function('integrate_pre_load', 'pr_pre_load');
add_integration_function('integrate_admin_areas', 'pr_admin_areas');
add_integration_function('integrate_load_permissions', 'pr_load_permissions');
add_integration_function('integrate_profile_areas', 'pr_profile_areas');
add_integration_function('integrate_actions', 'pr_actions');

if ($context['uninstalling'])
{
	remove_integration_function('integrate_pre_include', '$sourcedir/Subs-PostRatings.php');
	remove_integration_function('integrate_pre_load', 'pr_pre_load');
	remove_integration_function('integrate_admin_areas', 'pr_admin_areas');
	remove_integration_function('integrate_load_permissions', 'pr_load_permissions');
	remove_integration_function('integrate_profile_areas', 'pr_profile_areas');
	remove_integration_function('integrate_actions', 'pr_actions');
}

if (!empty($ssi))
	echo 'Database installation complete!';

?>