<?php

if (file_exists(dirname(__FILE__) . '/SSI.php') && !defined('SMF'))
{
	$ssi = true;
	require_once(dirname(__FILE__) . '/SSI.php');
}
elseif (!defined('SMF'))
	exit('<b>Error:</b> Cannot install - please verify you put this in the same place as SMF\'s index.php.');

if (!array_key_exists('db_add_column', $smcFunc))
	db_extend('packages');

// the columns to add
$column_add = array();
$column_indexes = array();

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'rating',
		'type' => 'decimal(10,8)',
		'default' => -1,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'total_ratings',
		'type' => 'int',
		'size' => 10,
		'null' => false,
		'default' => 0,
		'unsigned' => true,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'ratings_enabled',
		'type' => 'tinyint',
		'size' => 4,
		'null' => false,
		'default' => 1,
	),
);

$column_add[] = array(
	'table' => 'messages',
	'info' => array(
		'name' => 'id_last_rating',
		'type' => 'int',
		'size' => 10,
		'null' => false,
		'default' => 0,
		'unsigned' => true,
	),
);

$column_indexes[] = array(
	'table' => 'messages',
	'info' => array(
		'type' => 'unique',
		'columns' => array('id_last_rating', 'id_msg'),
	),
);

// add the columns into topics table
$column_add[] = array(
	'table' => 'topics',
	'info' => array(
		'name' => 'is_ratings',
		'type' => 'tinyint',
		'size' => 4,
		'null' => false,
		'default' => 0,
	),
);

$column_indexes[] = array(
	'table' => 'topics',
	'info' => array(
		'type' => 'key',
		'columns' => array('is_ratings'),
	),
);

// now for the message ratings table
$post_ratings_columns[] = array(
	'name' => 'id_rating',
	'type' => 'int',
	'size' => 11,
	'unsigned' => true,
	'null' => false,
	'auto' => true,
);
$post_ratings_columns[] = array(
	'name' => 'id_msg',
	'type' => 'int',
	'size' => 11,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);
$post_ratings_columns[] = array(
	'name' => 'id_member',
	'type' => 'mediumint',
	'size' => 8,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);

$post_ratings_columns[] = array(
	'name' => 'date',
	'type' => 'int',
	'size' => 10,
	'null' => false,
	'default' => 0,
	'unsigned' => true,
);
$post_ratings_columns[] = array(
	'name' => 'value',
	'type' => 'tinyint',
	'size' => 2,
	'null' => false,
);

// Indexes
$post_ratings_columns_indexes[] = array(
	'type' => 'primary',
	'columns' => array('id_rating')
);

$post_ratings_columns_indexes[] = array(
	'type' => 'key',
	'columns' => array('id_msg')
);

// now for the message ratings table
$post_ratings_columns2[] = array(
	'name' => 'id_board',
	'type' => 'smallint',
	'size' => 5,
	'unsigned' => true,
	'auto' => true,
);
$post_ratings_columns2[] = array(
	'name' => 'variable',
	'type' => 'varchar',
	'size' => 60,
);
$post_ratings_columns2[] = array(
	'name' => 'value',
	'type' => 'varchar',
	'size' => 255,
);

// Indexes
$post_ratings_columns_indexes2[] = array(
	'type' => 'primary',
	'columns' => array('id_board', 'variable')
);

// Create the tables...
$smcFunc['db_create_table']('{db_prefix}log_message_ratings', $post_ratings_columns, $post_ratings_columns_indexes, array(), 'ignore');
$smcFunc['db_create_table']('{db_prefix}ratings_settings', $post_ratings_columns2, $post_ratings_columns_indexes2, array(), 'ignore');

// Insert the columns into the tables specified.
if (!empty($column_add))
	foreach ($column_add as $column)
		$smcFunc['db_add_column']('{db_prefix}' . $column['table'], $column['info']);

// Add the indexes...
if (!empty($column_indexes))
	foreach ($column_indexes as $index)
		$smcFunc['db_add_index']('{db_prefix}' . $index['table'], $index['info'], array(), 'ignore');

$convert_settings = array(
	'rate1' => !isset($modSettings['post_ratings_th1']) ? 20 : (int) $modSettings['post_ratings_th1'],
	'rate2' => !isset($modSettings['post_ratings_th2']) ? 50 : (int) $modSettings['post_ratings_th2'],
	'rate3' => !isset($modSettings['post_ratings_th3']) ? 75 : (int) $modSettings['post_ratings_th3'],
	'rate4' => !isset($modSettings['post_ratings_th4']) ? 90 : (int) $modSettings['post_ratings_th4'],
	'text1' => !isset($modSettings['post_ratings_text1']) ? 'Poor' : (int) $modSettings['post_ratings_text1'],
	'text2' => !isset($modSettings['post_ratings_text2']) ? 'Fair' : (int) $modSettings['post_ratings_text2'],
	'text3' => !isset($modSettings['post_ratings_text3']) ? 'Good' : (int) $modSettings['post_ratings_text3'],
	'text4' => !isset($modSettings['post_ratings_text4']) ? 'Great' : (int) $modSettings['post_ratings_text4'],
	'text5' => !isset($modSettings['post_ratings_text5']) ? 'Excellent' : (int) $modSettings['post_ratings_text5'],
);

// Insert the settings
$smcFunc['db_insert']('ignore',
		'{db_prefix}settings',
		array(
			'variable' => 'string', 'value' => 'string',
		),
		array(
			array('post_ratings_enable', '0'),
			array('post_ratings_first_post', '0'),
			array('post_ratings_autoenable_topic_mode', '0'),
			array('post_ratings_auto_expanded', '1'),
			array('post_ratings_disable_locked_topic', '0'),
			array('post_ratings_limit_count', '0'),
			array('post_ratings_show_quantity', '1'),
			array('post_ratings_show_lastratedtime', '1'),
			array('post_ratings_posts_enable_icon', '1'),
			array('post_ratings_posts_delete_icon', '1'),
			array('post_ratings_image_rated', 'Themes/default/images/post_ratings/star.png'),
			array('post_ratings_image_nonrated', 'Themes/default/images/post_ratings/star_off.png'),
			array('post_ratings', serialize(array(
				array(
					'threshold' => $convert_settings['rate1'],
					'text' => $convert_settings['text1'],
				),
				array(
					'threshold' => $convert_settings['rate2'],
					'text' => $convert_settings['text2'],
				),
				array(
					'threshold' => $convert_settings['rate3'],
					'text' => $convert_settings['text3'],
				),
				array(
					'threshold' => $convert_settings['rate4'],
					'text' => $convert_settings['text4'],
				),
				array(
					'threshold' => '100',
					'text' => $convert_settings['text5'],
				),
			))),
			array('post_ratings_not_rated', '[b]Not Yet Rated![/b]'),
			array('post_ratings_not_expanded_rate', '[b]Post Rating Options (Rate this post)[/b]'),
			array('post_ratings_not_expanded_edit', 'Post Rating Options'),
			array('post_ratings_layout_style', '0'),
		),
		array('variable')
	);

updateSettings(array(
	'show_rating_profile' => 1,
	'show_rating_recent' => 1,
	'show_rating_search' => 1,
	'show_top_ten_ratings' => 1,
	'show_top_ten_raters' => 1,
	'can_rating_cat_collapse' => 1,
	'show_rating_post_time' => 1,
	'show_rating_post_author' => 1,
));

if (!empty($ssi))
	echo 'Database installation complete!';

?>