<?php

// Settings...
$txt['post_ratings_settings'] = 'Post Ratings';
$txt['post_ratings_settings_desc'] = 'Here you can change the settings of Post Ratings';
$txt['post_ratings_images'] = 'Images';
$txt['post_ratings_text'] = 'Strings';
$txt['post_ratings_text_desc'] = 'BBC and smileys allowed for all text fields in here.';
$txt['post_ratings_settings_title'] = 'Post Ratings Settings';
$txt['post_ratings_settings_general'] = 'General';
$txt['post_ratings_settings_boards'] = 'Boards';
$txt['post_ratings_enable'] = 'Enable Post Ratings';
$txt['post_ratings_category'] = 'Categories to enable ratings on';
$txt['post_ratings_category_desc'] = 'Selected categories override board options if no boarrd is selected.';
$txt['post_ratings_category_all'] = '[All categories]';
$txt['post_ratings_boards'] = 'Boards to enable ratings on';
$txt['post_ratings_board_none'] = '[No boards]';
$txt['post_ratings_autoenable_topic_mode'] = 'Automatic Ratings Setting for Topics';
$txt['post_ratings_autoenable_disabled_new'] = 'New Topics (Disabled)';
$txt['post_ratings_autoenable_new_topics'] = 'New Topics (Enabled)';
$txt['post_ratings_autoenable_disabled_all'] = 'All Topics (Disabled)';
$txt['post_ratings_autoenable_all_topics'] = 'All Topics (Enabled)';
$txt['post_ratings_first_post'] = 'Enable First Post Rating';
$txt['post_ratings_topic_only'] = 'Limit rating to the first post';
$txt['post_ratings_topic_only_desc'] = 'First post rating must be enabled for this to take effect.';
$txt['post_ratings_mi'] = 'Show first post rating on message index';
$txt['post_ratings_layout_style'] = 'Post Ratings Layout';
$txt['post_ratings_layout_style_inner'] = 'Inner Style';
$txt['post_ratings_layout_style_header'] = 'Header Style';
$txt['post_ratings_auto_expanded'] = 'Automatically Expanded in posts';
$txt['post_ratings_auto_expanded_disabled'] = 'Disabled';
$txt['post_ratings_auto_expanded_enabled'] = 'Enabled (All posts)';
$txt['post_ratings_auto_expanded_enabled_new'] = 'Enabled (Unrated posts)';
$txt['post_ratings_disable_locked_topic'] = 'Disable Rating posts in Locked Topics';
$txt['post_ratings_limit_count'] = 'Ratings Limit per post<div class="smalltext">0 to disable</div>';
$txt['post_ratings_show_quantity'] = 'Show Rating Quantities in posts';
$txt['post_ratings_show_lastratedtime'] = 'Show Last Rated Time in posts';
$txt['post_ratings_posts_enable_icon'] = 'Show enable/disable icons in posts<div class="smalltext">Only shows if permission is set.</div>';
$txt['post_ratings_posts_delete_icon'] = 'Show delete ratings icon in posts<div class="smalltext">Only shows if permission is set.</div>';
$txt['post_ratings_image_nonrated'] = 'Non-Rated';
$txt['post_ratings_image_rated'] = 'Rated';
$txt['post_ratings_image_halfrated'] = 'Half-Rated';
$txt['post_ratings'] = 'Rating text';
$txt['post_ratings_number'] = 'Rating #';
$txt['post_ratings_not_rated'] = 'Not Rated';
$txt['post_ratings_level'] = 'Ratings Strength Level<div class="smalltext">Levels from 1 - 10 only (no decimals allowed)</div>';
$txt['post_ratings_not_expanded_rate'] = 'Rating Options Title (Not Yet Rated)';
$txt['post_ratings_not_expanded_edit'] = 'Rating Options Title (Edit/Delete Rating)';
$txt['show_rating_profile'] = 'Show ratings in profile view';
$txt['show_rating_recent'] = 'Show ratings in recent view';
$txt['post_ratings_forum_integration'] = 'SMF Integration';
$txt['show_rating_search'] = 'Show ratings in search view';
$txt['post_ratings_forum_integration_desc'] = 'Choose other areas of SMF to include ratings. Ratings cannot be modified in these areas and only feature the inline display, eg. below the post content.';
$txt['show_top_ten_ratings'] = 'Show top ten ratings';
$txt['show_top_ten_raters'] = 'Show top ten raters';
$txt['post_ratings_bi_integration'] = 'Board index integration';
$txt['can_rating_cat_collapse'] = 'Collapsible';
$txt['rating_cat'] = 'Category to choose';
$txt['rating_cat_none'] = '[No category]';
$txt['rating_cat_desc'] = 'Pick a category to display the top rated post next to.';
$txt['rating_cat_position'] = 'Position';
$txt['before'] = 'Before';
$txt['after'] = 'After';
$txt['rating_board'] = 'Board to choose';
$txt['rating_board_none'] = '[No board]';
$txt['rating_board_desc'] = 'Pick a board to display the top rated post next to. This option has no effect if a dedicated category is made.';
$txt['rating_board_position'] = 'Position';
$txt['show_rating_post_time'] = 'Show post time';
$txt['show_rating_post_author'] = 'Show post author';
$txt['show_rating_bi'] = 'Show rating count';

// Admin Help Strings...
$txt['post_ratings_level_help'] = 'Sets the maximum overall total ratings score for a post.  The higher the Strength Level, the higher the overall score of that post.  This helps to give a better, more precise, and unique percentage of that posts overall rating, however, if it is set too high could take a lot of ratings until a post reaches the next/prev percentage value.  <br />Maximum value per post will be changed to (Ratings Strength Level &times; 100).  The maximum value allowed is 10, which would make the maximum overall ratings score value for a post to be no more than 1,000.  Please note, this will not have any effect on the current rating of posts, except either make it more precise in its percentage total (higher value), or less precise (lower value).  If you change this, and have many posts that have been rated already, this could take awhile to update since changes will take effect in all rated posts on the forum.';
$txt['post_ratings_not_expanded_rate_help'] = 'Sets what to display if the user hasn\'t rated this post yet and has the permission to rate posts.  User clicks on this area at the bottom of posts in order to expand or collapse the ability to rate the post.';
$txt['post_ratings_not_expanded_edit_help'] = 'Sets what to display if the user already rated this post and has the permission to edit and/or delete their own ratings.  User clicks on this area at the bottom of posts in order to expand or collapse the ability to edit/delete their rating for that post.';
$txt['managepostratings_settings_description'] = 'Here you can set all settings involved when rating posts within a topic.';
$txt['post_ratings_images_help'] = 'All filepaths for images are relative to your SMF Root directory.  The Rated image is the image that displays the rating, while the Non-Rated image shows opportunity for improvement.<br /><br /><strong>For Example</strong>:  A Rating of 2 will display, from left to right, 2 Rated images and 3 Non-Rated images.';
$txt['post_ratings_text_help'] = 'These are the strings that will show to the right of all rated/nonrated images when a member hovers their mouse over any of the Non-Rated images.  This describes the rating they are about to give that post.  The Not Rated string is what shows by default when a new post is created, and will stay that way until it gets rated by a member.  You can blank this string out if you don\'t want anything to show for posts that have not been rated yet.';
$txt['post_ratings_first_post_help'] = 'Allows the first post within a topic to be rated.  Note, if disabled, deleting ratings and enabling/disabling ratings from the first post will be unallowed as well.';
$txt['post_ratings_mi_help'] = 'The rating of the first post gets displayed on the message index with this option. You can also sort the list of topics by their rating as well. Note that this applies even if the ratings are not limited to the first post.';
$txt['post_ratings_layout_style_help'] = 'Choose the Layout Style for showing ratings within posts.  Currently only 2 styles to choose from, Inner Style and Header Style.  While Inner Style allows for all BBC and Smileys, Header Style is somewhat restricted on certain types of BBC tags, however, it does support smileys.';
$txt['post_ratings_auto_expanded_help'] = 'Choose the view you want for when users rate posts just underneath the actual post in topics.  <br /><br /><strong>Disabled</strong> - Rating Options for New Ratings and Edit/Delete Ratings will always show as collapsed.<br /><strong>Enabled (All Posts)</strong> - Rating Options for New Ratings and Edit/Delete Ratings will always show as expanded.<br /><strong>Enabled (Unrated posts)</strong> - Rating Options for Edit/Deleting Ratings will always show as collapsed, and will be expanded if the user hasn\'t rated that post yet.';
$txt['post_ratings_disable_locked_topic_help'] = 'Sets whether to disable ratings in a topic that is locked and/or becomes locked.  If this option is set and a topic is locked, the topic must be unlocked before any posts can be rated in that topic.  Also, topic must be unlocked before the topic and/or any posts can be enabled/disabled for rating as well as ratings being deleted from posts and/or the topic.';
$txt['post_ratings_limit_count_help'] = 'Sets the limit for ratings in all posts.  Once this limit is reached, no new ratings will be accepted.  Users who have already rated this post will not be able to delete their rating once the post has reached its limit of ratings, they will be able to edit their rating only.  This will not remove any current ratings from posts whether it exceeds this amount or not.  Set to 0 to disable this feature.';
$txt['post_ratings_show_quantity_help'] = 'Sets whether to show the rating quantities inside of posts that have post ratings enabled.  This is the text that gets displayed to the right of the top portion of the post that says, <em>Rated <strong>x</strong> times</em> (where x is the number of times the post has been rated).';
$txt['post_ratings_show_lastratedtime_help'] = 'Sets whether to show the last rated time inside of posts that have post ratings enabled and have atleast 1 rating in it.  This is the text that gets displayed just underneath the time that the post was created, which is underneath the title of the actual post itself.';
$txt['post_ratings_posts_enable_icon_help'] = 'Sets whether to display the Enabled/Disabled star icon within the upper right-hand side of posts when viewing topics by a user who has permission to enable/disable own or any posts.  If the topic is disabled for rating, these icons will not be displayed regardless of this setting.<br /><br /><strong>Please Note</strong>: Users who have the permission to enable/disable posts can still enable or disable posts from being rated when creating or modifying a post.';
$txt['post_ratings_posts_delete_icon_help'] = 'Sets whether to display the Delete All Ratings from this post icon within the upper right-hand side of posts when viewing topics by a user who has permission to Administrate Ratings.  If the topic or post is disabled for rating, this icon will not be displayed regardless of this setting.<br /><br /><strong>Please Note</strong>: Users who have the ability to Administrate Post Ratings can still delete all ratings within that post by modifying the post (if able to modify posts).';
$txt['post_ratings_autoenable_topic_mode_help'] = 'This is the default Enable option for Ratings in a topic on your forum.  It is recommended that you keep it either on New Topics (Disabled) or New Topics (Enabled).  <br /><strong>Please Note</strong>: If you select either of the All Topics options it will enable/disable ratings in ALL topics on your forum in all Categories and Boards, Selecting New Topics (Enabled) or New Topics (Disabled), will <strong>NOT</strong> enable/disable any of the topics currently on your forum, will just enable/disable New topics from now on.  The following lists the available options and a brief description of each option:<br /><br /><strong>New Topics (Disabled)</strong>:  This option will simply have the enable ratings option unchecked when creating new topics, if the permission is enabled (own/any) within the topics section of the Board Profile Permissions.<br /><strong>New Topics (Enabled)</strong>:  This option will enable, by default, post ratings for all new topics from now on, if the permission is enabled (own/any) within the topics section of the Board Profile Permissions.  This option will not change any of the preexisting topics that have been enabled/disabled for rating.<br /><strong>All Topics (Disabled)</strong>:  This option will set all of your current topic ratings to disabled as well as have the default Enable mode for new topics and/or preexisting topics unchecked.  So users will have to check the box manually to enable post ratings in topics.<br /><strong>All Topics (Enabled)</strong>:  This option will set all of your current topic ratings to Enabled as well as have the default Enable mode for new topics and/or preexisting topics checked.  So users will have to uncheck the box manually to disable post ratings in topics (if they have permission to do so).';
$txt['post_ratings_threshold_help'] = 'Sets the required percentage for each rating.  You can make it harder to reach a rating of 5 stars by setting the Rating of 4 to a higer number.  The higher you set the percentages for each rating, the harder it will be to obtain the next rating.<br /><br />Please Note:  It is not recommended to set higher ratings with a lower percentage.  For Example:  Setting a Rating of 2 to 20% and Rating of 1 to 45%.  This will produce undesirable results.';

// Permissions
$txt['permissiongroup_postratings'] = 'Post Ratings';
$txt['permissiongroup_simple_postratings'] = 'Post Ratings';
$txt['permissionname_postratings_view'] = 'View Ratings';
$txt['permissionhelp_postratings_view'] = 'Allows the member to view the current rating of a post within a topic.';
$txt['permissionname_postratings_rate'] = 'Rate Posts';
$txt['permissionhelp_postratings_rate'] = 'Allows the member to rate posts within a topic.';
$txt['permissionname_postratings_edit_own_rating'] = 'Edit Own Rating';
$txt['permissionhelp_postratings_edit_own_rating'] = 'Allows the member to edit their own rating for all posts that they have rated.';
$txt['permissionname_postratings_delete_own_rating'] = 'Delete Own Rating';
$txt['permissionhelp_postratings_delete_own_rating'] = 'Allows the member to delete their own rating for posts they have already rated.';
$txt['permissionname_postratings_administrate'] = 'Administrate Post Ratings';
$txt['permissionhelp_postratings_administrate'] = 'Allows these members to delete all member ratings for any post and/or topic, including their own.';
$txt['permissionname_ratings_enable_topic'] = 'Enable Post Ratings';
$txt['permissionhelp_ratings_enable_topic'] = 'Enables the topic creator to allow this topic to be rated if <strong>Own topic</strong> is selected.  If <strong>Any topic</strong> is selected, will allow these users to enable/disable Post Ratings for all topics.<br /><br /><strong>Please Note</strong>: Only effects topics in boards that use this specific Boards Permission Profile.';
$txt['permissionname_ratings_enable_topic_own'] = 'Own topic';
$txt['permissionname_simple_ratings_enable_topic_own'] = 'Can Enable Post Ratings in their own created topics';
$txt['permissionname_ratings_enable_topic_any'] = 'Any topic';
$txt['permissionname_simple_ratings_enable_topic_any'] = 'Can Enable Post Ratings in any topics';
$txt['permissionname_ratings_enable_post'] = 'Disable Ratings';
$txt['permissionhelp_ratings_enable_post'] = 'Enables the post creator within that topic, if Post Ratings are enabled within that topic, to disable this post to be rated if <strong>Own post</strong> is selected.  If <strong>Any post</strong> is selected, will allow these users to disable/enable Post Ratings for all posts within a topic that has Post Ratings enabled.<br /><br /><strong>Please Note</strong>: Only effects posts in boards that use this specific Boards Permission Profile. Also, good to know, that, if both (Any and Own) are unselected, ratings for that post will be on by default, and these users will not be able to disable it for their own posts within that topic (if topic ratings are enabled for that topic).';
$txt['permissionname_ratings_enable_post_own'] = 'Own post';
$txt['permissionname_simple_ratings_enable_post_own'] = 'Can Disable Ratings in their own posts';
$txt['permissionname_ratings_enable_post_any'] = 'Any post';
$txt['permissionname_simple_ratings_enable_post_any'] = 'Can Disable Ratings in any post';

// Additional Options... (in topic creation)
$txt['ratings_enable_topic'] = 'Enable Post Ratings in topic.';
$txt['ratings_enable_post'] = 'Disable Ratings in this post.';
$txt['ratings_delete_post'] = 'Delete all ratings in this post.';
$txt['ratings_delete_topic'] = 'Delete all ratings in this topic.';

// Error Handling
$txt['cannot_postratings_administrate'] = 'Sorry, you do not have permission to delete any post ratings!';
$txt['post_ratings_disabled'] = 'Sorry, but post ratings has been disabled!';
$txt['cannot_postratings_rate'] = 'Sorry, you do not have permission to rate a post!';
$txt['cannot_postratings_delete_own_rating'] = 'Sorry, you are not allowed to delete your own ratings!';
$txt['postratings_not_able_to_rate'] = 'Sorry, you are not able to rate this post.';
$txt['postratings_not_able_to_edit'] = 'Sorry, you are not able to edit your rating for this post.';
$txt['ratings_delete_no_msg_topic'] = 'Seems that no post or topic was specified.  Can\'t delete ratings without a post or topic to delete ratings from.';
$txt['ratings_delete_post_err'] = 'Unable to delete ratings from within this post.  Possible causes:  This post hasn\'t been rated yet, the topic in which this post is in, is locked, topic is disabled for rating, and/or post is disabled for rating.  Please fix these possible causes and try again.';
$txt['ratings_delete_topic_err'] = 'Unable to delete ratings within this topic.  Possible causes:  All posts within this topic haven\'t been rated yet, the topic is locked, and/or the topic is disabled for ratings. Please fix these possible causes and try again.';
$txt['postratings_enable_disable_error'] = 'Unable to Enable/Disable this post for rating.  Possible causes:  The topic is locked, and/or the topic is disabled for rating.';
$txt['cannot_ratings_enable_disable'] = 'Sorry, you are not allowed to enable or disable this post or topic!';
$txt['topic_enable_disable_error'] = 'Unable to Enable/Disable this topic for rating.  Please unlock the topic and try again!';
$txt['postratings_no_rating_selected'] = 'Unable to rate this post due to no rating being selected.';
$txt['postratings_no_topic_selected'] = 'Unable to rate this post due to no topic being selected.';
$txt['postratings_no_post_selected'] = 'Unable to rate this post due to no message being selected.';
$txt['postratings_rate_post_error'] = 'Unable to rate this post.  Possible causes:  The topic is locked, ratings is disabled in this topic and/or post, the ratings limit for this post has been reached.  Please fix these possible causes and try again.';
$txt['postratings_delete_post_error'] = 'Unable to delete ratings from this post.  Possible causes:  The topic is locked, ratings is disabled in this topic and/or post, the ratings limit for this post has been reached.';

// Administration and Permissions for Displaying in Topics
$txt['postratings_show'] = 'Show all post ratings!';
$txt['delete_topic_ratings'] = 'Delete All Ratings';
$txt['disable_topic_ratings'] = 'Disable Ratings';
$txt['enable_topic_ratings'] = 'Enable Ratings';

$txt['delete_ratings_post'] = 'Delete';
$txt['delete_ratings_post_alt'] = 'Delete all ratings in this post';
$txt['enable_ratings_post'] = 'Enabled';
$txt['enable_ratings_post_alt'] = 'Click to disable ratings in this post';
$txt['disable_ratings_post'] = 'Disabled';
$txt['disable_ratings_post_alt'] = 'Click to enable ratings in this post';

// AutoExpanded
$txt['postratings_rate_it'] = 'Rate this post:';
$txt['postratings_your_rating'] = 'Your Rating:';

$txt['postratings_count1'] = 'Rated';
$txt['postratings_count2'] = 'time';
$txt['postratings_count3'] = 'times';
$txt['last_rated_time'] = '<strong>Last Rated on:</strong>';
$txt['remove_rating'] = 'Remove Your Rating';

// Profile
$txt['rated_posts'] = 'Rated Posts';
$txt['rating_disable'] = 'Disable ratings in posts';

// BoardIndex
$txt['ratings'] = 'ratings';
$txt['top_rated_post'] = 'Top Rated Post';

// Stats
$txt['top_ten_ratings'] = 'Top Ten Ratings';
$txt['top_ten_raters'] = 'Top Ten Raters';

?>